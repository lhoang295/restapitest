var mongoose = require('mongoose');
var config = require('../config');


var userSchema = mongoose.Schema({
    name: String,
    password: String
});

var User = mongoose.model('User', userSchema);

module.exports = User;

